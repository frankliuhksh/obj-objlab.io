+++
title = "Space Runner"
+++
The first game I made.
Controls:
- WASD or arrows to move
- Q or space to fire laser
- 1 to pause/unpause
- 2 for fullscreen

Music from <a href="https://bensound.com">bensound.com</a>.
<canvas id="canvas" width="4000" height="2000"></canvas>
<script src="/a/js/spacerunner.js"></script>