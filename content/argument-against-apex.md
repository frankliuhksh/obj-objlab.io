+++
title = "Apex Bad"
+++

# An Argument Against Apex (and other expensive Minecraft hosting):

This is an argument against using expensive Minecraft hosting providers (like [Apex](https://apexminecrafthosting.com/)) in favor of more powerful and/or less expensive alternatives. Apex's pricing is _kinda sketch_ - the reoccuring price is much higher than the price for the first month - so I'll be using the reoccuring price for these comparisons.

First, some of Apex's pricing:

Apex's EX series has:

-   A 4.7GHz CPU
-   4 Dedicated threads
-   15GB RAM
-   Costs \$80/mo

Apex's 8GB plan has:

-   An unknown CPU
-   An unknown number of threads (probably 1 or 2)
-   8GB RAM
-   Costs $40/mo

### Fig. A: Why is pricing comparable to cloud compute?

[Linode](https://www.linode.com) is a cloud-compute host (you can run anything on their servers, and they're meant for anything computationally intensive).

The funny thing is, Linode actually has comparable pricing

-   Unknown CPU
-   6 (non-dedicated) threads
-   16GB RAM
-   Costs \$80/mo

It's not a good sign if you're a Minecraft host and your product is as expensive as literal could compute...

### Fig. B: An extremely cheap alternative:

[Pebblehost](https://pebblehost.com) is a server host I found a while ago (don't remember exactly how I found it). Let's see how they compare to Apex:

Dedicated server:

-   4.8GHz CPU
-   4 Dedicated threads
-   16GB RAM
-   Costs \$73/mo

Non-dedicated server:

-   4.8GHz CPU
-   Unknown number of threads (Probably 1 or 2)
-   16GB RAM
-   Costs \$36/mo

Pebblehost also has the same features as Apex (DDOS protection, support, etc.).

### Conclusion:

_Please_ don't buy Apex. You could get much better performance per dollar by going for Pebblehost or another cheap host instead. Pebblehost is just an example here, I'll compile a list of cheap hosts below (in order of cheap to more expensive):

Disclaimer: I haven't actually used any of these apart from pebblehost, the others could be scams for all I know

Cheap:

-   [SparkedHost](https://sparkedhost.com/budget-minecraft-hosting)
-   [Pebblehost](https://pebblehost.com/minecraft/budget)
-   [PloxHost](https://plox.host/minecraft-hosting)
-   [ShockByte](https://shockbyte.com/games/minecraft-server-hosting)

Less cheap, but not _expensive_:

-   [Bisect Hosting](https://www.bisecthosting.com/minecraft-server-hosting.php)

Expensive:

-   [Server.pro](https://server.pro/)
-   [Apex](https://apexminecrafthosting.com/pricing/)

### Sources:

[Apex's Pricing](https://apexminecrafthosting.com/pricing/)\
[Pebblehost's Pricing](https://pebblehost.com/minecraft/budget)\
[Linode's Pricing](https://www.linode.com/pricing/)
