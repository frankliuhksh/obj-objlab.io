///////////////////
//// VARIABLES ////
///////////////////

// Get the canvas and drawing context, set the cursor to working
var canvas = document.getElementById("canvas");
var game = canvas.getContext("2d");
canvas.style.cursor = "wait";

// Make the canvas the size of the window
canvas.style.width = "100%";
canvas.style.height = canvas.style.width / 2;

var time = 0;
var timestep = 30;
var start = 0;
var end = 0;
var i;
var stopped = true;

// Images
var asteroid = new Image();
asteroid.src = "/a/spacerunner/asteroid.png";
var spaceship = new Image();
spaceship.src = "/a/spacerunner/spaceship.png";
var alienship = new Image();
alienship.src = "/a/spacerunner/alienship.png";
var beamboss = new Image();
beamboss.src = "/a/spacerunner/beamboss.png";
var background = new Image();
background.src = "/a/spacerunner/background.jpg";
var explosion = new Image();
explosion.src = "/a/spacerunner/explosion.png";
var star = new Image();
star.src = "/a/spacerunner/star.png";
var backgroundx = 0;

var obstacles = [];
var obstaclexspeed = 20;
var explosions = [];
obstacles.push(new Obstacle("asteroid"));
var stars = [];
for (i = 0; i < 200; i++) {
    stars.push(new Star());
}

var scoreamount = 2;
var score = 1000;
var thousands = 0;

// Display loading screen
text(["Loading...", "This might take a while..."]);

var music = new Audio("/a/spacerunner/music.mp3");
music.loop = true;
music.play();

///////////////
//// PLAYER ////
////////////////

var player = {
    x: 50,
    y: 1000,
    width: 257,
    height: 200,
    up: false,
    down: false,
    left: false,
    right: false,
    firing: false,
    bullet: new Bullet(canvas.width, 0, "blue"),
    speed: 1.5,
    energy: 1000,
    shield: 100,
    toplimit: 0,
    bottomlimit: canvas.height - 200,
    leftlimit: 0,
    rightlimit: canvas.width - 257,
    yspeed: 0,
    xspeed: 0,
    xaccel: 0,
    yaccel: 0,
    laser: new Audio("/a/spacerunner/laser.wav"),
    explosion: new Audio("/a/spacerunner/explosion.wav"),

    update: function () {
        this.y += this.yspeed;
        this.x += this.xspeed;
        this.xspeed += this.xaccel;
        this.yspeed += this.yaccel;

        if (this.up) {
            this.yaccel = -this.speed;
        } else if (!this.up && this.yspeed < 0) {
            this.yaccel = this.speed;
        }
        if (this.down) {
            this.yaccel = this.speed;
        } else if (!this.down && this.yspeed > 0) {
            this.yaccel = -this.speed;
        }
        if (this.yspeed > -4 && this.yspeed < 4 && !this.up && !this.down) {
            this.yspeed = 0;
        }

        if (this.left) {
            this.xaccel = -this.speed;
        } else if (!this.left && this.xspeed < 0) {
            this.xaccel = this.speed;
        }
        if (this.right) {
            this.xaccel = this.speed;
        } else if (!this.right && this.xspeed > 0) {
            this.xaccel = -this.speed;
        }
        if (this.xspeed > -4 && this.xspeed < 4 && !this.left && !this.right) {
            this.xspeed = 0;
        }

        if (this.y < this.toplimit) {
            this.yspeed = 0;
            this.yaccel = 0;
            this.y = this.toplimit;
        }
        if (this.y > this.bottomlimit) {
            this.yspeed = 0;
            this.yaccel = 0;
            this.y = this.bottomlimit;
        }
        if (this.x < this.leftlimit) {
            this.xspeed = 0;
            this.xaccel = 0;
            this.x = this.leftlimit;
        }
        if (this.x > this.rightlimit) {
            this.xspeed = 0;
            this.xaccel = 0;
            this.x = this.rightlimit;
        }

        if (this.energy < 1000) {
            this.energy += 5;
        }
        if (this.shield < 100 && this.energy > 10) {
            this.shield += 0.5;
            this.energy -= 10;
        }
        if (this.shield <= 0) {
            this.shield = 0;
            reset();
        }

        if (this.firing) {
            this.fire();
        }

        this.bullet.update();
    },

    render: function () {
        game.drawImage(spaceship, this.x, this.y);
        game.fillStyle = "blue";
        game.fillRect(
            this.x,
            this.y + this.height + 25,
            (this.shield / 100) * this.width,
            25
        );
        game.fillStyle = "yellow";
        game.fillRect(
            this.x,
            this.y + this.height + 60,
            (this.energy / 1000) * this.width,
            25
        );
        this.bullet.render();
    },

    fire: function () {
        if (this.energy > 50 && this.bullet.dead) {
            this.bullet.x = this.x + 100;
            this.bullet.y = this.y + 90;
            this.bullet.dead = false;
            this.energy -= 50;
            this.bullet.xspeed = 150;
            this.laser.play();
        }
    },
};

//////////////////////
//// BULLET CLASS ////
//////////////////////

function Bullet(x, y, color) {
    this.x = x;
    this.y = y;
    this.width = 100;
    this.height = 20;
    this.xspeed = 50;
    this.color = color;
    this.dead = false;

    this.die = () => {
        this.dead = true;
        this.x = -1000;
        this.y = 0;
    };

    this.update = () => {
        if (!this.dead) {
            this.x += this.xspeed;

            if (this.x > canvas.width || this.x < -this.width) {
                this.die();
            }
        }
    };

    this.render = () => {
        if (!this.dead) {
            game.fillStyle = this.color;
            game.fillRect(this.x, this.y, this.width, this.height);
        }
    };
}

////////////////////////
//// OBSTACLE CLASS ////
////////////////////////

function Obstacle(type) {
    this.type = type;
    this.width = 0;
    this.height = 0;
    this.image = asteroid;
    this.explosion = new Audio("/a/spacerunner/explosion.wav");
    this.maxhealth = 1;
    this.health = 1;
    this.index = obstacles.length;

    this.basicupdate = () => {
        this.x -= this.speed;
        if (this.x < -this.width) {
            this.randomize();
        } else if (collision(this, player)) {
            this.randomize();
            this.explosion.play();
            player.shield -= 25;
        } else if (collision(this, player.bullet)) {
            this.health -= 1;
            player.bullet.die();
            this.explosion.play();
            if (this.health <= 0) {
                this.health = this.maxhealth;
                this.randomize();
                this.x += 10000;
                if (this.type == "asteroid") {
                    score += 100;
                } else if (this.type == "alienship") {
                    score += 1000;
                } else if (this.type == "beamboss") {
                    score += 5000;
                }
            }
        }
    };

    this.basicrender = () => {
        game.drawImage(this.image, this.x, this.y);
        game.fillStyle = "red";
        game.fillRect(
            this.x,
            this.y + this.height + 25,
            (this.health / this.maxhealth) * this.width,
            25
        );
    };

    this.randomize = () => {
        this.x = canvas.width;
        this.speed = Math.floor(Math.random() * 10 + obstaclexspeed - 5);
        if (Math.floor(Math.random() * 10) === 0) {
            this.y = player.y;
        } else {
            this.y = Math.floor(Math.random() * (canvas.height - this.height));
        }
    };

    if (this.type == "asteroid") {
        this.width = 150;
        this.height = 150;
        this.image = asteroid;
        this.maxhealth = 1;
        this.health = 1;

        this.randomize = () => {
            this.x = canvas.width;
            this.speed = Math.floor(Math.random() * 10 + 15);
            this.yspeed = Math.floor(Math.random() * 10 - 4);
            this.y = Math.floor(Math.random() * (canvas.height - this.height));
        };

        this.update = () => {
            this.basicupdate();
            this.y += this.yspeed;

            if (this.y < -200 || this.y > canvas.height) {
                this.randomize();
            }

            for (this.i = 0; this.i < obstacles.length; this.i++) {
                if (
                    obstacles[this.i].type == "asteroid" &&
                    obstacles[this.i].index != this.index &&
                    collision(this, obstacles[this.i])
                ) {
                    this.explosion.play();
                    explosions.push(
                        new Explosion(
                            this.x,
                            this.y,
                            (-this.speed + -obstacles[i].speed) / 2,
                            (this.yspeed + obstacles[i].yspeed) / 2
                        )
                    );
                    this.randomize();
                    obstacles[this.i].randomize();
                }
            }
        };

        this.render = () => {
            this.basicrender();
        };
    } else if (this.type == "alienship") {
        this.width = 281;
        this.height = 200;
        this.image = alienship;
        this.bullet = new Bullet(-51, 0, "lime");
        this.bullet.xspeed = -100;
        this.reloadtimer = 0;
        this.laser = new Audio("/a/spacerunner/laser.wav");
        this.maxhealth = 2;
        this.health = 2;

        this.update = () => {
            this.basicupdate();
            this.bullet.update();
            this.reloadtimer -= 1;

            if (collision(this.bullet, player.bullet)) {
                this.bullet.xspeed = 50;
                player.bullet.xspeed = 100;
            } else if (collision(this.bullet, player)) {
                this.bullet.die();
                this.explosion.play();
                player.shield -= 50;
            }

            if (this.reloadtimer <= 0 && this.x < 4000) {
                this.bullet.xspeed = -100;
                this.bullet.x = this.x - 100;
                this.bullet.y = this.y + 90;
                this.bullet.dead = false;
                this.reloadtimer = Math.floor(Math.random() * 10 + 46);
                this.laser.play();
            }
        };

        this.render = () => {
            this.basicrender();
            this.bullet.render();
        };
    } else if (this.type == "beamboss") {
        this.x = 3400;
        this.y = 750;
        this.yspeed = 2;
        this.width = 545;
        this.height = 500;
        this.image = beamboss;
        this.reloadtimer = 50;
        this.randomize = () => {};
        this.beam = new Audio("/a/spacerunner/beam.wav");
        this.maxhealth = 10;
        this.health = 10;

        this.randomize = () => {
            obstacles.splice(obstacles.indexOf(this), 1);
        };

        this.drawbeam = () => {
            game.fillStyle =
                "rgba(0, 255, 0, " + (this.reloadtimer - 50) / 10 + ")";
            game.beginPath();
            game.moveTo(this.x, this.y + this.width / 2);
            game.lineTo(0, this.y);
            game.lineTo(0, this.y + this.width);
            game.lineTo(this.x, this.y + this.width / 2);
            game.fill();
        };

        this.update = () => {
            this.y += this.yspeed;
            this.reloadtimer -= 1;

            if (this.y + this.height > canvas.height || this.y < 0) {
                this.yspeed *= -1;
            }

            if (
                this.reloadtimer > 50 &&
                collision(player, {
                    x: player.x,
                    y: this.y,
                    width: canvas.width,
                    height: this.height,
                })
            ) {
                player.y = this.y + this.height / 2 - player.height / 2;
                player.xaccel = player.speed / 2;
            }

            if (collision(this, player)) {
                player.shield = -1;
            }

            if (collision(this, player.bullet)) {
                this.health -= 1;
                player.bullet.die();
                this.explosion.play();
                if (this.health <= 0) {
                    this.health = this.maxhealth;
                    this.randomize();
                    this.x += 10000;
                }
            }

            if (this.reloadtimer <= 0) {
                this.reloadtimer = Math.floor(Math.random() * 10 + 66);
                this.beam.play();
            }
        };

        this.render = () => {
            this.basicrender();
            if (this.reloadtimer > 50) {
                this.drawbeam();
            }
        };
    }

    if (this.type !== "beamboss") {
        this.randomize();
    }
}

////////////////////
//// STAR CLASS ////
////////////////////

function Star() {
    this.image = star;
    this.size = 25;

    this.update = () => {
        this.x -= this.speed;

        if (this.x < -this.size) {
            this.randomize();
        }
    };

    this.render = () => {
        game.drawImage(this.image, this.x, this.y, this.size, this.size);
    };

    this.randomize = () => {
        this.x = canvas.width;
        this.speed = Math.random() + 1;
        this.y = Math.floor(Math.random() * canvas.height);
        this.size = this.speed * 10;
    };

    this.randomize();
    this.x = Math.floor(Math.random() * canvas.width);
}

/////////////////////////
//// EXPLOSION CLASS ////
/////////////////////////

function Explosion(x, y, xspeed, yspeed) {
    this.index = explosions.length;
    this.x = x;
    this.y = y;
    this.xspeed = xspeed - 5;
    this.yspeed = yspeed - 5;
    this.width = 50;
    this.height = 50;
    this.image = explosion;

    this.update = () => {
        this.width += 10;
        this.height += 10;
        this.x += this.xspeed;
        this.y += this.yspeed;

        if (collision(this, player)) {
            player.shield -= 25;
            explosions.splice(explosions.indexOf(this), 1);
        }

        if (this.width >= 500) {
            for (i = 0; i < explosions.length; i++) {
                if (explosions[i].index > this.index) {
                    explosions[i].index -= 1;
                }
            }
            explosions.splice(explosions.indexOf(this), 1);
        }
    };

    this.render = () => {
        game.drawImage(this.image, this.x, this.y, this.width, this.height);
    };
}

///////////////////////////
//// DRAWING FUNCTIONS ////
///////////////////////////

function fill() {
    game.drawImage(background, backgroundx, 0);
}

function text(lines) {
    game.fillStyle = "gray";
    game.fillRect(0, 0, canvas.width, canvas.height);
    game.fillStyle = "black";
    game.font = "100px courier";
    for (i = 0; i < lines.length; i++) {
        game.fillText(lines[i], 25, 125 + 100 * i);
    }
}

function pausedtext() {
    fill();
    game.fillStyle = "red";
    game.font = "750px courier";
    game.fillText("Paused...", 25, 1200);
}

function collision(o1, o2) {
    // Checks if 2 objects collide
    this.left1 = o1.x;
    this.right1 = o1.x + o1.width;
    this.top1 = o1.y;
    this.bottom1 = o1.y + o1.height;
    this.left2 = o2.x;
    this.right2 = o2.x + o2.width;
    this.top2 = o2.y;
    this.bottom2 = o2.y + o2.height;
    if (
        this.left1 < this.right2 &&
        this.right1 > this.left2 &&
        this.top1 < this.bottom2 &&
        this.bottom1 > this.top2
    ) {
        return true;
    } else {
        return false;
    }
}

function reset() {
    // Resets game (when player dies)
    stoploop();
    setTimeout(restofreset, 250);
}

function restofreset() {
    fill();
    game.fillStyle = "red";
    game.font = "250px courier";
    game.fillText("You died...", 25, 250);
    game.fillText("(press 1", 25, 500);
    game.fillText(" to resume)", 25, 750);
    game.fillStyle = "lime";
    game.fillText("Your score was:", 25, 1000);
    game.fillText(score, 25, 1250);
    time = 0;
    player.y = 1000;
    player.x = 50;
    player.up = false;
    player.down = false;
    player.left = false;
    player.right = false;
    player.shield = 100;
    player.energy = 1000;
    player.xspeed = 0;
    player.yspeed = 0;
    player.xaccel = 0;
    player.yaccel = 0;
    player.bullet = new Bullet(canvas.width, 0, "blue");
    obstacles = [new Obstacle("asteroid")];
    explosions = [];
    thousands = 1;
    score = 1000;
    for (i = 0; i < obstacles.length; i++) {
        if (obstacles[i].y > 400 && obstacles[i].y < 600) {
            if (obstacles[i].y < 500) {
                obstacles[i].y = 400;
            } else {
                obstacles[i].y = 600;
            }
        }
    }
}

function update() {
    // Updates variables
    for (i = 0; i < stars.length; i++) {
        stars[i].update();
    }
    for (i = 0; i < obstacles.length; i++) {
        obstacles[i].update();
    }
    for (i = 0; i < explosions.length; i++) {
        explosions[i].update();
    }
    player.update();

    if (score - thousands * 1000 > 1000) {
        thousands += 1;

        if (thousands > 50) {
            obstaclexspeed += 1;
        }

        if (thousands < 7 && thousands != 1) {
            obstacles.push(new Obstacle("asteroid"));
        } else if (thousands == 7 || thousands == 10) {
            obstacles.push(new Obstacle("alienship"));
        } else if (thousands == 15) {
            obstacles.push(new Obstacle("beamboss"));
        }
    }

    if (backgroundx < -4000) {
        backgroundx = 0;
    }
    backgroundx -= 1;

    score += scoreamount;
}

function render() {
    // Renders to the screen
    fill();
    for (i = 0; i < stars.length; i++) {
        stars[i].render();
    }
    for (i = 0; i < obstacles.length; i++) {
        obstacles[i].render();
    }
    for (i = 0; i < explosions.length; i++) {
        explosions[i].render();
    }
    player.render();
    game.fillStyle = "red";
    game.font = "100px courier";
    game.fillText("Score : " + score, 25, 100);
    game.fillText("Energy: " + Math.floor(player.energy), 25, 200);
    game.fillText("Shield: " + Math.floor(player.shield) + "%", 25, 300);
}

/*--------------------------------*\
| The fixed timestep loop - this   |
| loop gets the time that each     |
| iteration takes and keeps the    |
| game running at the same rate.   |
| Rendering to the screen always   |
| takes much longer than updating  |
| the objects, so if the game      |
| is behind, the objects are       |
| updated several times each       |
| frame, simulating the lost time. |
\*--------------------------------*/

function loop() {
    end = performance.now();
    time += Math.min(1000, end - start);
    start = end;
    while (time > timestep) {
        update();
        time -= timestep;
    }
    render();
    if (!stopped) {
        requestAnimationFrame(loop);
    }
}

function startloop() {
    requestAnimationFrame(loop);
    stopped = false;
    start = performance.now();
}

function stoploop() {
    stopped = true;
}

function terminate() {
    music.pause();
    stoploop();

    music = undefined;

    time = undefined;
    timestep = undefined;
    start = undefined;
    end = undefined;
    stopped = undefined;
    backgroundx = undefined;

    obstacles = undefined;
    obstaclexspeed = undefined;
    explosions = undefined;

    scoreamount = undefined;
    score = undefined;
    thousands = undefined;
}

document.terminate = () => {
    terminate();
};

/////////////////////////
//// EVENT LISTENERS ////
/////////////////////////

// event.preventDefault() prevents the default thing from happening (like space and arrows scrolling down, or F5 reloading).

addEventListener("keydown", function (event) {
    // What happens when the key is pressed down
    if (event.keyCode == 87 || event.keyCode == 38) {
        event.preventDefault();
        player.up = true;
    } else if (event.keyCode == 83 || event.keyCode == 40) {
        event.preventDefault();
        player.down = true;
    } else if (event.keyCode == 65 || event.keyCode == 37) {
        event.preventDefault();
        player.left = true;
    } else if (event.keyCode == 68 || event.keyCode == 39) {
        event.preventDefault();
        player.right = true;
    } else if (event.keyCode == 81 || event.keyCode == 32) {
        event.preventDefault();
        player.firing = true;
    } else if (event.keyCode == 49) {
        event.preventDefault();
    } else if (event.keyCode == 50) {
        event.preventDefault();
        if (canvas.requestFullscreen) {
            canvas.requestFullscreen();
        } else if (canvas.webkitRequestFullScreen) {
            canvas.webkitRequestFullScreen();
        }
    }
});

addEventListener("keyup", function (event) {
    // What happens when the key is released
    if (event.keyCode == 87 || event.keyCode == 38) {
        player.up = false;
    } else if (event.keyCode == 83 || event.keyCode == 40) {
        player.down = false;
    } else if (event.keyCode == 65 || event.keyCode == 37) {
        player.left = false;
    } else if (event.keyCode == 68 || event.keyCode == 39) {
        player.right = false;
    } else if (event.keyCode == 81 || event.keyCode == 32) {
        event.preventDefault();
        player.firing = false;
    } else if (event.keyCode == 49) {
        if (stopped) {
            startloop();
        } else {
            stoploop();
            setTimeout(pausedtext, 250);
        }
    }
});

canvas.style.cursor = "crosshair";
requestAnimationFrame(loop);
start = performance.now();
setTimeout(pausedtext, 500);
