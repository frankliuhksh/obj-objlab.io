let nav = document.getElementById("nav");

document.onclick = (e) => {
    var a;
    if (e.target.tagName == "A") {
        a = e.target;
    } else {
        a = e.target.closest("a");
    }

    if (a) {
        e.preventDefault();
        nav.style.pointerEvents = "none";
        document.body.style.opacity = "0";
        document.body.ontransitionend = () => {
            location = a.href;
        };
    }
};
